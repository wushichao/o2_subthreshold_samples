# O2_subthreshold_samples

O2 sub-threshold samples for comparison. The posterior samples and power spectral density are stored in the format of an ASCII file. 

The 3 new triggers have large posterior samples, so they are in `.gz` format. Unzip with `gunzip [file.gz]`.


## Posterior Samples
Files in the format of `[GW]_[Approximant]_posterior_samples.dat` contain the full posterior samples, including but not limited to these:

* `logl`: log likelihood

* `mc` or `mc_source`: chirp mass

* `eta`: mass ratio

* `a1z`: aligned spin of primary

* `a2z`: aligned spin of secondary

* `ra`: right ascension

* `dec`: declination

* `psi`: polarization angle

* `theta_jn`: inclination

* `phase`: orbital phase

* `time`: arrival time at each IFO

* `distance` or `dist`: luminosity distance
    
### Example Codes


```
import numpy
pos=numpy.genfromtxt('GW170727_SEOBNRv4_ROMpseudoFourPN_posterior_samples.dat',names=True)
eta=pos['pos']
```
One may also you `panda` or other packages one prefers. 

## Power Spectral Density (PSD) for each interferometer(IFO) 
Files in the format of `[GW]_[Approximant]_[IFO]_PSD.dat` contain the PSDs, first column is frequency (Hz), second column is PSD (1/Hz).

Calculated using BayesWave PSD (for 4 seconds of data)
